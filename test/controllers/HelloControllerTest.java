package controllers;

import domain.HelloResponse;
import org.junit.Test;
import play.Application;
import play.inject.guice.GuiceApplicationBuilder;
import play.libs.Json;
import play.mvc.Http;
import play.mvc.Result;
import play.test.Helpers;
import play.test.WithApplication;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.matchesPattern;
import static org.junit.Assert.assertEquals;
import static play.mvc.Http.Status.NOT_IMPLEMENTED;
import static play.mvc.Http.Status.OK;
import static play.test.Helpers.*;

public class HelloControllerTest extends WithApplication {

    @Override
    protected Application provideApplication() {
        return new GuiceApplicationBuilder().build();
    }

    @Test
    public void test_index() {
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(GET)
                .uri("/");

        Result result = route(app, request);
        assertEquals(OK, result.status());
    }

    @Test
    public void test_get() {
        //given
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(GET)
                .uri("/hello");
        //when
        Result result = route(app, request);
        //then
        assertEquals(OK, result.status());
    }

    @Test
    public void test_getById() {
        //given
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(GET)
                .uri("/hello/123");
        //when
        Result result = route(app, request);
        //then
        assertEquals(OK, result.status());
        assertEquals("123", Helpers.contentAsString(result));
    }

    @Test
    public void test_post() {
        //given
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(POST)
                .bodyJson(Json.toJson(new HelloResponse(123, "foo")))
                .uri("/hello");
        //when
        Result result = route(app, request);
        //then
        assertEquals(CREATED, result.status());
        assertThat( Json.parse(Helpers.contentAsString(result)).get("id").asText(), matchesPattern("[-]?\\d+"));
        assertThat( Json.parse(Helpers.contentAsString(result)).get("description").asText(), equalTo("foo"));
    }

    @Test
    public void test_delete() {
        //given
        Http.RequestBuilder request = new Http.RequestBuilder()
                .method(DELETE)
                .uri("/hello/123");
        //when
        Result result = route(app, request);
        //then
        assertEquals(NOT_IMPLEMENTED, result.status());
    }
}
