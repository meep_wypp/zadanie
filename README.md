# ZADANIE #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This repository provides four REST services using Play Framework with Java
* Version: 1.0

### How do I get set up? ###

* Prequisities
  * download and install GIT from https://git-scm.com/downloads
  * download and install SBT from http://www.scala-sbt.org/download.html
  * [optional] download and instal CURL from https://curl.haxx.se/download.html
  * verify that SBT is working properly

```
> sbt about
Java HotSpot(TM) 64-Bit Server VM warning: ignoring option MaxPermSize=256m; support was removed in 8.0
WARN: No sbt.version set in project/build.properties, base directory: d:\
[warn] Executing in batch mode.
[warn]   For better performance, hit [ENTER] to switch to interactive mode, or
[warn]   consider launching sbt without any commands, or explicitly passing 'shell'
[info] Loading global plugins from C:\Users\meep_wypp\.sbt\0.13\plugins
[info] Set current project to root (in build file:/D:/)
[info] This is sbt 0.13.15
[info] The current project is {file:/D:/}root 0.1-SNAPSHOT
[info] The current project is built against Scala 2.10.6
[info] Available Plugins: sbt.plugins.IvyPlugin, sbt.plugins.JvmPlugin, sbt.plugins.CorePlugin, sbt.plugins.JUnitXmlReportPlugin, sbt.plugins.Giter8TemplatePlugin
[info] sbt, sbt plugins, and build definitions are using Scala 2.10.6
```

* Dependencies: Java 8+, Play Framework 2.5+
* Clone repository 

```
> git clone https://bitbucket.org/meep_wypp/zadanie.git
Cloning into 'zadanie'...
remote: Counting objects: 95, done.
remote: Compressing objects: 100% (80/80), done.
remote: Total 95 (delta 21), reused 0 (delta 0)
Unpacking objects: 100% (95/95), done.

> cd zadanie
```

* Running server from the command line

```
> sbt run
Java HotSpot(TM) 64-Bit Server VM warning: ignoring option MaxPermSize=256m; support was removed in 8.0
[warn] Executing in batch mode.
[warn]   For better performance, hit [ENTER] to switch to interactive mode, or
[warn]   consider launching sbt without any commands, or explicitly passing 'shell'
[info] Loading global plugins from C:\Users\meep_wypp\.sbt\0.13\plugins
[info] Loading project definition from D:\IntelliJ.workspace\zadanko\project
[info] Updating {file:/D:/IntelliJ.workspace/zadanko/project/}zadanko-build...
[info] Resolving org.fusesource.jansi#jansi;1.4 ...
[info] Done updating.
[info] Set current project to zadanie (in build file:/D:/IntelliJ.workspace/zadanko/)
[info] Updating {file:/D:/IntelliJ.workspace/zadanko/}root...
[info] Resolving jline#jline;2.14.3 ...
[info] Done updating.

--- (Running the application, auto-reloading is enabled) ---

[info] p.c.s.AkkaHttpServer - Listening for HTTP on /0:0:0:0:0:0:0:0:9000

(Server started, use Enter to stop and go back to the console...)
```

* RESTful services
       
| Method | Request | Example
| --------|---------|---------|
| GET  | localhost:9000/hello   |```curl -I -X GET http://localhost:9000/hello```|
| GET | localhost:9000/hello/:id |```curl -I -X GET http://localhost:9000/hello/1410```|
| POST | localhost:9000/hello |```curl -X POST http://localhost:9000/hello -H "Content-Type: application/json" -d "{\"description\":\"foo\"}"```|
| DELETE | localhost:9000/hello/:id |```curl -I -X DELETE http://localhost:9000/hello/2017```|

### Who do I talk to? ###

* przemyslaw.wypierowski@meelogic.com
