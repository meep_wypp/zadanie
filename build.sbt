name := """zadanie"""
organization := "com.meelogic.meepwypp"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.12.2"

libraryDependencies += guice
libraryDependencies += javaWs
libraryDependencies += "org.hamcrest" % "hamcrest-junit" % "2.0.0.0"