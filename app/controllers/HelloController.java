package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.google.inject.Inject;
import domain.HelloRequest;
import domain.HelloResponse;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import services.HelloService;
import services.LocationService;

import java.util.Optional;
import java.util.Random;

/**
 * This controller contains an actions to handle HTTP requests
 * to the application's home page.
 */
public class HelloController extends Controller {

    @Inject
    private HelloService helloService;

    @Inject
    private LocationService locationService;

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    public Result index() {
        return ok(views.html.index.render());
    }

    /**
     * An action that renders HTTP status code for URL given in the <code>application.conf</code>.<p>
     * The configuration in the <code>routes</code> file means that this method will be called
     * when the application receives a <code>GET</code> request with a path of <code>localhost:9000/hello</code>
     *
     * @return action result
     */
    public Result get() {
        int httpStatusCode = helloService.getHello();
        return status(httpStatusCode, Integer.toString(httpStatusCode));
    }

    /**
     * An action that renders HTTP status code given by the request.<p>
     * The configuration in the <code>routes</code> file means that this method will be called
     * when the application receives a <code>GET</code> request with a path of <code>localhost:9000/hello/:id</code>
     * @param id resource identifier
     * @return action result
     */
    public Result getById(Integer id) {
        return ok(Integer.toString(id));
    }

    /**
     * An action that renders JSON response body consisting of a single key-value pair of random integer and
     * textual description.<p>
     * The configuration in the <code>routes</code> file means that this method will be called
     * when the application receives a <code>POST</code> request with a path of <code>localhost:9000/hello</code>
     *
     * @return action result
     */
    public Result post() {
        return Optional.ofNullable(request().body().asJson())
                .map(jsonBody -> locationService.postHello( Json.fromJson(jsonBody, HelloRequest.class) ))
                .orElseGet(() -> badRequest("JSON payload not found."));
    }

    /**
     * An action that deliberately renders HTTP status {@link Http.Status#NOT_IMPLEMENTED}.
     * The configuration in the <code>routes</code> file means that this method will be called
     * when the application receives a <code>DELETE</code> request with a path of <code>localhost:9000/hello</code>
     * @param id resource identifier
     * @return action result
     */
    public Result deleteById(Integer id) {
        return status(Http.Status.NOT_IMPLEMENTED);
    }
}
