package services;

import domain.HelloRequest;
import domain.HelloResponse;
import play.libs.Json;
import play.mvc.Result;

import java.util.Random;

import static play.mvc.Results.created;

public class LocationService {

    public Result postHello(HelloRequest helloRequest) {
        return created(Json.toJson(
                new HelloResponse(
                        new Random().nextInt(), helloRequest.getDescription()
                )
        ));
    }
}
