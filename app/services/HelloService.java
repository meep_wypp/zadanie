package services;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import play.api.Configuration;
import play.libs.ws.WSClient;
import play.libs.ws.WSRequest;
import play.libs.ws.WSResponse;
import play.mvc.Http;

import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;

@Singleton
public class HelloService {

    @Inject
    WSClient ws;

    @Inject
    Configuration conf;

    public int getHello() {
        WSRequest request = ws.url(conf.underlying().getString("url"));
        CompletionStage<WSResponse> responsePromise = request.get();
        try {
            return responsePromise.toCompletableFuture().get().getStatus();
        } catch (InterruptedException | ExecutionException e) {
            return Http.Status.INTERNAL_SERVER_ERROR;
        }
    }
}
