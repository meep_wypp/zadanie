package domain;

public class HelloRequest {
    private String description;

    public HelloRequest() {
    }

    public HelloRequest(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}
